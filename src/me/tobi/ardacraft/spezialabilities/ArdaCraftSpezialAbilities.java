package me.tobi.ardacraft.spezialabilities;

import org.bukkit.plugin.java.JavaPlugin;

public class ArdaCraftSpezialAbilities extends JavaPlugin{
	
	private static CooldownManager manager = new CooldownManager();
	private static ArdaCraftSpezialAbilities plugin;
	
	public static CooldownManager getCooldownManager() {
		return manager;
	}
	
	@Override
	public void onEnable() {
		plugin = this;
	}
	
	public static ArdaCraftSpezialAbilities getPlugin() {
		return plugin;
	}
	
}
